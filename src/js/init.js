'use strict';

var $ = window.jQuery;

//ON DOCUMENT READY
$(document).ready(function() {

    //Location slider
    if($('.location-slider').length) {
        var locationSlider = tns({
            container: '.location-slider',
            fixedWidth: 222,
            swipeAngle: false,
            speed: 400,
            gutter: 20,
            nav: false,
            controlsText: ['','<img src="/images/icon-arrow-right.png">'],
            mouseDrag: true,
            responsive: {
                0: {
                    fixedWidth: 125,
                    gutter: 30
                }, 
                1440: {
                    fixedWidth: 173
                }
              }
        });
    }

    //Customer reviews slider
    if($('.customer-reviews-slider').length) {
        var reviewsSlider = tns({
            container: '.customer-reviews-slider',
            items: 3,
            swipeAngle: false,
            speed: 400,
            gutter: 40,
            nav: false,
            mouseDrag: true,
            controlsText: ['<img src="/images/icon-left-large.png">','<img src="/images/icon-right-large.png">'],
            responsive: {
                0: {
                  items: 1,
                  controls: true
                },
                768: {
                    items: 2,
                    controls: true
                }, 
                1025: {
                    items: 3
                }
              }
        });
    }


    //Form - datepicker field
    $( "#datepicker" ).datepicker({
        firstDay: 1,
        minDate: 1,
        beforeShow: function( input, inst){
            $(inst.dpDiv).addClass('datepicker');
        }
    });

    $("#datepicker").datepicker("setDate", new Date());

    var Menu = function () {
        $('.js-toggle-menu').on('click', this.showHideMobileNav.bind(this));
        $('.js-toggle-menu-locations').on('click', this.checkIfPrimaryMenyIsOpened.bind(this));
        $('.js-toggle-search').on('click', this.showHideSearch.bind(this));
    };

    Menu.prototype = {

        closeSearchItemWhenClickOutside: function (e) {
            e.stopPropagation();
            if ($(e.target).parents('.js-search-item').length === 0) {
                $('.js-search-item').removeClass('is-active');
                $('body').removeClass('search-menu-active');
            }
        },

        showHideMobileNav: function () {
            $('body').toggleClass('menu-is-active');
        },

        showHideSearch: function() {
            $('body').toggleClass('search-is-active');
        },

        checkIfPrimaryMenyIsOpened: function() {
            if ($('body').hasClass('menu-is-active')) {
                this.toggleLocationsNav();
            }
        },

        toggleLocationsNav: function () {
            $('.header-nav-primary').toggleClass('is-hidden');
            $('.header-nav-secondary').toggleClass('is-active');
        }
    };

    new Menu();

    var Filters = function () {
        $('.js-filter-trigger').on('click', this.toggleFilterItem.bind(this));
        $('.js-toggle-fields').on('click', this.toggleFields.bind(this));
        $('.js-close-filter').on('click', this.closeFilter.bind(this));
        $(document).on('click', this.closeFilterWhenClickOutside.bind(this));
        $('.js-input-checkbox').on('click', this.writeNumberOfSelectedItems.bind(this));
        $('.js-filter-clear').on('click', this.uncheckAllFilters.bind(this));
    };

    Filters.prototype = {

        toggleFilterItem: function (e) {
            var clickedLink = $(e.currentTarget);
            var clickedLinkParent = clickedLink.parent();
            clickedLinkParent.toggleClass('is-active');
        },

        toggleFields: function() {
            $('.form-fields-hidden').toggleClass('is-visible');
        },

        closeFilterWhenClickOutside: function (e) {
            e.stopPropagation();
            if ($(e.target).parents('.js-filter').length === 0) {
               this.hideFilter();
            }
        },

        hideFilter: function() {
            $('.js-filter').removeClass('is-active');
        },

        closeFilter: function() {
            this.hideFilter();
        },
        
        countNumberOfSelectedItems: function() {
            return $('.js-input-checkbox:checked').length;
        },

        writeNumberOfSelectedItems: function() {
            var numberOfCheckedCheckboxes = this.countNumberOfSelectedItems();
            numberOfCheckedCheckboxes > 0 
                ? $('.js-filter-trigger').text('Selected: ' + numberOfCheckedCheckboxes)
                : $('.js-filter-trigger').text('Choose some preferences');
        },

        uncheckAllFilters: function() {
            $('.js-input-checkbox').each(function() { 
                this.checked = false; 
            });
            this.writeNumberOfSelectedItems();
        }
    };

    new Filters();
});


//WINDOW ONLOAD
$(window).on("load",function() {
    
});

var iconNormal = 'images/pin1.png';
var iconSelected = 'images/pin.svg';  
var iconLarge = 'images/icon-map-large.svg';

//Map with multiple properties
var MapWithProperties = function() {
    if($('#map').length) {
        this.getPropertiesDataFromMarkup();
        this.initMap();
    }
}

MapWithProperties.prototype = {
    map: null,
    markers: [],
    properties: [],
    propertiesData: $(".js-property-item"),
    
    getPropertiesDataFromMarkup: function () {
        for (var i = 0; i < this.propertiesData.length; i++) {
            var lat = this.propertiesData[i].dataset.lat;
            var lng = this.propertiesData[i].dataset.lng;
            var name = this.propertiesData[i].dataset.name;
            var image = this.propertiesData[i].dataset.image;
            var propertyId = this.propertiesData[i].dataset.propertyId;
            this.properties.push([
                '<div class="map-infobox">' +
                '<img src="' + image + '" class="map-infobox-image" />' +
                '<h5 class="map-infobox-title">' + name + '</h5>' +
                '<a href="' + propertyId + '" class="map-infobox-link">View space</a>'+
                '</div>'
                ,lat, lng
            ]
            );
        }
    },
    
    initMap: function () {
        this.map = new google.maps.Map(document.getElementById('map'), {
            zoom: 10,
            styles: [
                {
                    featureType: "all",
                    stylers: [
                        { saturation: -80 },
                        { lightness: 50 }
                    ]
                }
            ],
            fullscreenControl: false,
            streetViewControl: false,
            mapTypeControl: false,
            zoomControlOptions: {
                position: google.maps.ControlPosition.RIGHT_BOTTOM
            }
        });
        this.setMarkers(this.map);
    },
    
    setMarkers: function (map) {
        var bounds = new google.maps.LatLngBounds();
        var markers = [];
        
        for (var i = 0; i < this.properties.length; i++) {
            var property = this.properties[i];
            var marker = new google.maps.Marker({
                position: {
                    lat: parseFloat(property[1]),
                    lng: parseFloat(property[2])
                },
                map: this.map,
                animation: google.maps.Animation.DROP,
                title: property[0],
                icon: iconNormal
            });
            bounds.extend(marker.getPosition());
            
            // set infobox for all markers
            var infowindow = new google.maps.InfoWindow({
                content: property[0]
            });
            
            marker.infobox = infowindow;
            markers.push(marker);
            
            // open marker on click
            google.maps.event.addListener(marker, 'click', (function (marker, i) {
                return function () {
                    // Close all other infoboxes
                    for (var j = 0; j < markers.length; j++) {
                        markers[j].infobox.close(map);
                        markers[j].setIcon(iconNormal);
                    }
                    // Open correct info box
                    markers[i].infobox.open(map, markers[i]);
                    markers[i].setIcon(iconSelected);
                }
            })(marker, i));
            
            //Close infowindow when click outside
            google.maps.event.addListener(map, "click", function (event) {
                markers.forEach(function (item) {
                    item.infobox.close(map);
                    item.setIcon(iconNormal);
                });
            });
        }
        
        this.map.fitBounds(bounds);
        this.markers = markers;

        // Add a marker clusterer to manage the markers.
        new MarkerClusterer(this.map, this.markers, {
            imagePath:
              "https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m", //TO DO - update icon
          });
    }
};

new MapWithProperties();
